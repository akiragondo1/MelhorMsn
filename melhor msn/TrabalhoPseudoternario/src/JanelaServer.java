
import conexao.ThreadServer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class JanelaServer extends Application {
	
	private Stage primaryStage;
	
	private TextArea txa_log;
	
	private Label lbl_port;
	private TextField txf_port;
	private Button btn_connect;
	
	private ThreadServer realizarConexao(int port, int user) {
		return new ThreadServer(port, user, data -> {
			Platform.runLater(() -> {
				String data_perm = data.toString();
				if(data.toString() == "Servidor iniciado.") {
					txa_log.setDisable(false);
					lbl_port.setDisable(true);
					txf_port.setDisable(true);
					btn_connect.setDisable(true);
				}
				else if (data.toString() == "Erro no servidor!") {
					txa_log.setDisable(true);
					lbl_port.setDisable(false);
					txf_port.setDisable(false);
					btn_connect.setDisable(false);
				}
				txa_log.appendText(data_perm + "\n");
			});
		});
	}
	
	private ThreadServer connection = null;
	
	private Parent createContent () throws Exception {
		GridPane main_gridpane = new GridPane();
		
		// Colunas do grid
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(10);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(20);
		ColumnConstraints column3 = new ColumnConstraints();
		column3.setPercentWidth(70);
		main_gridpane.getColumnConstraints().addAll(column1,column2,column3);
		
		txa_log = new TextArea("");
		txa_log.setPrefHeight(250);
		txa_log.setDisable(true);
		txa_log.setEditable(false);
		lbl_port = new Label("Port: ");
		txf_port = new TextField("7777");
		txf_port.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d*")) {
	            	txf_port.setText(newValue.replaceAll("[^\\d]", ""));
	            }
	        }
	    });
		btn_connect = new Button("Criar Servidor");
		btn_connect.setPrefWidth(100);
		btn_connect.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				// Conectar
				int connPort = Integer.parseInt( txf_port.getText() );
				try {
					if(connection != null)
					{
						connection.closeConnection();
					}
					
					connection = realizarConexao(connPort, 10);
					connection.startConnecion();
				} catch (Exception e) {
					txa_log.appendText("Erro: " + e.getMessage() + "\n");
					connection = null;
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		GridPane.setConstraints(txa_log, 0, 1, 3, 1, HPos.CENTER, VPos.CENTER);
		GridPane.setConstraints(lbl_port, 0, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
		GridPane.setConstraints(txf_port, 1, 0, 1, 1, HPos.CENTER, VPos.CENTER);
		GridPane.setConstraints(btn_connect, 2, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
		main_gridpane.getChildren().addAll(txa_log,lbl_port,txf_port,btn_connect);
		
		return main_gridpane;
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		this.primaryStage = primaryStage;
		
		Scene scene = new Scene(createContent(), 500, 300);
		
		this.primaryStage.setTitle("MSN Server - UTFPR ver.");
		this.primaryStage.setScene(scene);
		this.primaryStage.setResizable(false);
		
		this.primaryStage.show();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}