import java.io.File;

import conexao.ThreadCliente;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;

public class JanelaCliente extends Application {

	private Stage primaryStage;
	
	// Elementos da barra superior de conex�o
	private Label lb_client_name;
	private Label lb_server_ip;
	private Label lb_server_port;
	
	private TextField txf_client_name;
	private TextField txf_server_ip;
	private TextField txf_server_port;
	
	private Button btn_connect;
	private Button shakeButton;
	
	// Elementos da janela principal
	private TextField txf_message;
	private TextArea txa_chat;
	
	private Button btn_send;
	
	private ThreadCliente realizarConexao(String ip, int port) {
		return new ThreadCliente(ip,port, data -> {
			Platform.runLater(() -> {
				String data_perm = data.toString();
				if(data.toString() == "Conectado.") {
					txa_chat.setDisable(false);
					txf_message.setDisable(false);
					btn_send.setDisable(false);
					shakeButton.setDisable(false);
				}
				else if (data.toString() == "Conexao interrompida.") {
					txa_chat.setDisable(true);
					txf_message.setDisable(true);
					btn_send.setDisable(true);
					shakeButton.setDisable(true);
				}
				else if (!data_perm.matches("[^@]*")) {
					data_perm = data_perm.replaceAll("[@]", "");
					sendShake(false);
				}
				txa_chat.appendText(data_perm + "\n");
			});
		});
	}
	
	private ThreadCliente connection = null;
	
	@Override
	public void init() throws Exception {
		//connection.startConnecion();
	}
	
	@Override
	public void stop() throws Exception {
		//connection.closeConnection();
	}
	
	public void sendMessage() {
		if(txf_message.getText().toString().isEmpty())
			return;
		String message = txf_client_name.getText()+": ";
		message += txf_message.getText();
		txf_message.clear();
		
		try {
			connection.send(message);
		}
		catch (Exception e) {
			txa_chat.appendText("Failed to send " + e.getMessage() + "\n");
		}
	}
	
	public void sendShake(boolean owner) {
		if(isShaking)
			return;
		
		try {
			if(owner) {
				String message = txf_client_name.getText()+" chamou a aten"+(ThreadCliente.use_utf_8 ? "��" : "ca")+"o!!!";
				connection.send("@" + message);
			}
				
			shakeStage(this.primaryStage);
		}
		catch (Exception e) {
			txa_chat.appendText("Failed to send " + e.getMessage() + "\n");
		}
	}
	
	private Media shaking_sound;
	private boolean isShaking = false;
	
	public void shakeStage(Stage stage) {
		if(isShaking)
			return;
		
		Timeline anim = new Timeline(new KeyFrame(Duration.seconds(0.05), new EventHandler<ActionEvent>() {
			int state = 0;
			
	        @Override
	        public void handle(ActionEvent actionEvent) {
	        	
	        	
	            if (state == 0) {
	            	stage.setX(stage.getX() + 3);
	            	stage.setY(stage.getY() + 3);
	            } else if (state == 1) {
	            	stage.setX(stage.getX() - 3);
	            	stage.setY(stage.getY() - 3);
	            } else if (state == 2) {
	            	stage.setX(stage.getX() + 3);
	            	stage.setY(stage.getY() - 3);
	        	} else if (state == 3) {
	        		stage.setX(stage.getX() - 3);
	        		stage.setY(stage.getY() + 3);
	            }
	            state++;
	            if(state == 4)
	            	state = 0;
	        }
		}));

		

    	anim.setCycleCount(40);
    	anim.setAutoReverse(false);
    	
    	MediaPlayer mediaPlayer = new MediaPlayer(this.shaking_sound);
    	
    	mediaPlayer.play();
    	anim.play();
    	isShaking = true;
    	
    	mediaPlayer.setOnEndOfMedia(new Runnable() {
			
			@Override
			public void run() {
				anim.stop();
				isShaking = false;
			}
		});
	}
	
	public Parent createContent() {
		// -------------------------- //
		// Grid principal
		GridPane main_gridpane = new GridPane();
		// Colunas do grid (1 para a caixa de enviar mensagem e outra para o bot�o)
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(70);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(20);
		ColumnConstraints column3 = new ColumnConstraints();
		column3.setPercentWidth(10);
		main_gridpane.getColumnConstraints().addAll(column1,column2,column3);
		// -------------------------- //
		// Grid da barra superior
		GridPane config_gridpane = new GridPane();
		// 5 colunas (2 labels, 2 txt field e 1 bot�o)
		ColumnConstraints column_lbl_name = new ColumnConstraints();
		column_lbl_name.setPercentWidth(10);
		ColumnConstraints column_txf_name = new ColumnConstraints();
		column_txf_name.setPercentWidth(18);
		ColumnConstraints column_lbl_ip = new ColumnConstraints();
		column_lbl_ip.setPercentWidth(10);
		ColumnConstraints column_txf_ip = new ColumnConstraints();
		column_txf_ip.setPercentWidth(18);
		ColumnConstraints column_lbl_port = new ColumnConstraints();
		column_lbl_port.setPercentWidth(10);
		ColumnConstraints column_txf_port = new ColumnConstraints();
		column_txf_port.setPercentWidth(18);
		ColumnConstraints column_btn_update = new ColumnConstraints();
		column_btn_update.setPercentWidth(16);
		config_gridpane.getColumnConstraints().addAll(column_lbl_name,column_txf_name,column_lbl_ip, column_txf_ip, column_lbl_port, column_txf_port, column_btn_update);
		// -------------------------- //
		// Criar os componentes
		lb_client_name = new Label("Nome: ");
		lb_server_ip = new Label("IP: ");
		lb_server_port = new Label("Port: ");
		txf_client_name = new TextField("Cliente");
		txf_server_ip = new TextField("127.0.0.1");
		txf_server_port = new TextField("7777");
		txf_server_port.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("\\d*")) {
	            	txf_server_port.setText(newValue.replaceAll("[^\\d]", ""));
	            }
	        }
	    });
		txf_message = new TextField();
		txf_message.setDisable(true);
		txf_message.textProperty().addListener(new ChangeListener<String>() {
	        @Override
	        public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
	            if (!newValue.matches("[^@]")) {
	            	txf_message.setText(newValue.replaceAll("[@]", ""));
	            }
	            else if (txf_message.getText().length() > 256) {
	                String s = txf_message.getText().substring(0, 256);
	                txf_message.setText(s);
	            }
	        }
	    });
		txa_chat = new TextArea("");
		txa_chat.setDisable(true);
		txa_chat.setEditable(false);
		txa_chat.setPrefHeight(250);
		btn_send = new Button("Enviar");
		btn_send.setDisable(true);
		btn_send.setPrefWidth(300);
		shakeButton = new Button("!");
		shakeButton.setDisable(true);
		shakeButton.setPrefWidth(300);
		btn_connect = new Button("Conectar");
		btn_connect.setPrefWidth(100);
		btn_connect.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {			
				btn_connect.setText("...");
				
				String connIp = txf_server_ip.getText();
				int connPort = Integer.parseInt( txf_server_port.getText() );
				
				try {
					if(connection != null)
						connection.closeConnection();
					
					connection = realizarConexao(connIp, connPort);
					connection.startConnecion();
				} catch (Exception e) {
					txa_chat.appendText("Erro: " + e.getMessage() + "\n");
					connection = null;
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				btn_connect.setText("Reconectar");
			}
		});
		txf_message.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(event.getCode() == KeyCode.ENTER)
					sendMessage();
			}
		});
		btn_send.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				sendMessage();
			}
		});
		
		// Alinhar componentes na barra superior
		GridPane.setConstraints(lb_client_name, 0, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
		GridPane.setConstraints(txf_client_name, 1, 0);
		GridPane.setConstraints(lb_server_ip, 2, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
		GridPane.setConstraints(txf_server_ip, 3, 0);
		GridPane.setConstraints(lb_server_port, 4, 0, 1, 1, HPos.RIGHT, VPos.CENTER);
		GridPane.setConstraints(txf_server_port, 5, 0);
		GridPane.setConstraints(btn_connect, 6, 0, 1, 1, HPos.CENTER, VPos.CENTER);
		config_gridpane.getChildren().addAll(lb_client_name,txf_client_name,lb_server_ip,txf_server_ip,lb_server_port,txf_server_port,btn_connect);
		// Alinhar componentes na janela
		GridPane.setConstraints(config_gridpane, 0, 0, 3, 1);
		GridPane.setConstraints(txa_chat, 0, 1, 3, 1);
		GridPane.setConstraints(txf_message, 0, 2);
		GridPane.setConstraints(btn_send, 1, 2);
		GridPane.setConstraints(shakeButton, 2, 2);
		
		main_gridpane.getChildren().addAll(config_gridpane,txa_chat,txf_message,btn_send,shakeButton);
		// -------------------------- //
		return main_gridpane;
		// -------------------------- //
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		this.primaryStage = primaryStage;
		
		Scene scene = new Scene(createContent(), 500, 300);
		
		String musicFile = "bin/assets/chamarAtencao.mp3";

		this.shaking_sound = new Media(new File(musicFile).toURI().toString());
		
		shakeButton.setOnAction(new EventHandler<ActionEvent>() {				
	            @Override
	            public void handle(ActionEvent actionEvent) {
	            	sendShake(true);
	            }
	        });
		
		this.primaryStage.setTitle("MSN - UTFPR ver.");
		this.primaryStage.setScene(scene);
		this.primaryStage.setResizable(false);
		
		this.primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
