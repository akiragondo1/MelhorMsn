package conexao;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.function.Consumer;

import pseudoternary.Pseudoternary;

public class ThreadCliente {
	public static boolean use_utf_8 = false;	
	private String ip;
	private int port;
	private ThreadConexao connThread = new ThreadConexao();
	private Consumer<Serializable> onReceiveCallback;
	
	public ThreadCliente(String ip, int port, Consumer<Serializable> onReceiveCallback) {
		this.ip = ip;
		this.port = port;
		this.onReceiveCallback = onReceiveCallback;
		connThread.setDaemon(true);
	}
	
	// Iniciar a thread de conexao
	public void startConnecion() throws Exception {
		connThread.start();
	}
	
	// Enviar algo para a saida dea dados
	public void send(Serializable data) throws Exception {
		//connThread.saida.writeObject(PseudoternaryWriter.PseudoternaryWriter(data.toString()));
		Serializable data_out = Pseudoternary.encode(data.toString(),use_utf_8);
		onReceiveCallback.accept(Pseudoternary.decode((byte[])data_out));
		connThread.saida.writeObject(data_out);
	}
	
	// Finalizar a thread de conexao
	public void closeConnection() throws IOException {
		connThread.entrada.close();
		connThread.saida.close();
		connThread.socket.close();
		Thread.currentThread().interrupt();
	}
	
	private class ThreadConexao extends Thread {
		private Socket socket;
		private ObjectInputStream entrada;
		private ObjectOutputStream saida;
		
		@Override
		public void run() {
			onReceiveCallback.accept("Conectando em " + ip + ":" + port);
			try (	Socket socket = new Socket(ip,port);
					ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
					ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
					) {
				// Tenta conectar
				this.socket = socket;

				// I/O
				this.entrada = in;
				this.saida = out;
				
				this.socket.setTcpNoDelay(true);
				
				onReceiveCallback.accept("Conectado.");
				
				// Fica esperando mensagens do servidor
				while(true) {
					Serializable data = (Serializable)entrada.readObject();
					
					//String data_cod = PseudoternaryReader.PseudoternaryReader(data.toString());
					String data_cod = Pseudoternary.decode((byte[])data);
					
					if(!Thread.currentThread().isInterrupted()) {
						onReceiveCallback.accept(data_cod);
					} else {
						break;
					}
				}
			}
			catch (Exception e) {
				onReceiveCallback.accept("Conexao interrompida.");
			}
		}
	}
}