package conexao;

import java.net.ServerSocket;
import java.net.Socket;

/*
 * http://www.devmedia.com.br/java-sockets-criando-comunicacoes-em-java/9465
 * http://makemobiapps.blogspot.com.br/p/multiple-client-server-chat-programming.html 
 * https://github.com/aboullaite/Multi-Client-Server-chat-application/tree/master/javaSwing-Server_Client/src/aboullaite
 * https://www.youtube.com/watch?v=VVUuo9VO2II
 * https://docs.oracle.com/javase/7/docs/api/java/net/Socket.html
 */
public class Servidor {
	
	public static void main(String[] args) {	
		
		// Porta da conex�o
		int port = 7777;
		// Capacidade de 10 usu�rios
		int num_clientes = 10;
		ThreadConexaoCliente[] threads_clientes = new ThreadConexaoCliente[num_clientes];
		
		ServerSocket server = null;
		try {
			System.out.println("Inicianlizando o servidor ...");
			server = new ServerSocket(port);
			System.out.println("Servidor iniciado, ouvindo a porta "+ port);
			
			// Loop do servidor esperando clientes
			while(true) {
				// Se achar um cliente ...
				Socket client = server.accept();
				// Procura um espa�o livre
				int i;
				for(i = 0; i < num_clientes; i++)
				{
					if(threads_clientes[i] == null) {
						System.out.println("Conectando novo cliente ...");
						threads_clientes[i] = new ThreadConexaoCliente(i,client,threads_clientes);
						threads_clientes[i].start();
						break;
					}
				}
				// Se n�o achar avisa erro
				if (i == num_clientes)
					System.out.println("Servidor cheio ...");
			}
		}
		catch(Exception e) {
			System.out.println("Erro no servidor: " + e.getMessage());
		}
	}
}
