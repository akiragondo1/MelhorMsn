package conexao;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.function.Consumer;

import pseudoternary.Pseudoternary;

public class ThreadConexaoCliente extends Thread {
	private Socket cliente;
	private ThreadConexaoCliente[] threads_clientes;
	private int thread_id;
	private ObjectOutputStream saida;
	private Consumer<Serializable> onReceiveCallback = null;
	
	public Socket getCliente() { return cliente; }
	
	public ThreadConexaoCliente(int id, Socket cliente, ThreadConexaoCliente[] threads_clientes) {
		this.thread_id = id;
		this.cliente = cliente;
		this.threads_clientes = threads_clientes;
	}
	
	public ThreadConexaoCliente(int id, Socket cliente, ThreadConexaoCliente[] threads_clientes,  Consumer<Serializable> onReceiveCallback) {
		this.thread_id = id;
		this.cliente = cliente;
		this.threads_clientes = threads_clientes;
		this.onReceiveCallback = onReceiveCallback;
	}
	
	@Override
	public void run() {
		ObjectInputStream entrada_cliente = null;
		try {
			Serializable mensagem;
			this.saida = new ObjectOutputStream(cliente.getOutputStream());
			entrada_cliente = new ObjectInputStream(cliente.getInputStream());
			
			while(true) {
				mensagem = (Serializable)entrada_cliente.readObject();
				String mensagem_string = Pseudoternary.decode((byte[])mensagem);
				if(! mensagem_string.isEmpty()) {
					
					if(this.onReceiveCallback != null)
						this.onReceiveCallback.accept(mensagem_string.toString());
					else
						System.out.println(mensagem_string.toString());
					
					for(int i = 0; i < threads_clientes.length; i++) {
						if(threads_clientes[i] != this && threads_clientes[i] != null)
						{
							threads_clientes[i].saida.flush();
							threads_clientes[i].saida.writeObject(mensagem);
						}
					}
					
				}
				if(mensagem_string.toString() == "/quit") {
					break;
				}
			}
			entrada_cliente.close();
			this.saida.close();
		}
		catch(Exception e) {
			
			if(this.onReceiveCallback != null)
				this.onReceiveCallback.accept("Excecao ocorrida na thread: " + e.getMessage());
			else
				System.out.println("Excecao ocorrida na thread: " + e.getMessage());
			
			try {
				entrada_cliente.close();
				this.saida.close();
				cliente.close();
			}
			catch(Exception ec) {}
			threads_clientes[this.thread_id] = null;
		}
	}
}
