package conexao;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.function.Consumer;

public class ThreadServer {
	private int port;
	private int num_client;
	private ThreadConexao connThread = new ThreadConexao();
	private ThreadConexaoCliente client[];
	private Consumer<Serializable> onReceiveCallback;
	
	public ThreadServer(int port, int num_client, Consumer<Serializable> onReceiveCallback) {
		this.port = port;
		this.num_client = num_client;
		this.onReceiveCallback = onReceiveCallback;
		connThread.setDaemon(true);
		client = new ThreadConexaoCliente[num_client];
	}
	
	// Iniciar a thread de conexao
	public void startConnecion() throws Exception {
		connThread.start();
	}
	
	// Finalizar a thread de conexao
	public void closeConnection() throws IOException {
		connThread.stopConnection();
	}
	
	private class ThreadConexao extends Thread {
		
		public void stopConnection() {
			Thread.currentThread().interrupt();
		}
		
		@Override
		public void run() {
			ServerSocket server = null;
			
			try {
				onReceiveCallback.accept("Inicianlizando o servidor ...");
				server = new ServerSocket(port);
				onReceiveCallback.accept("Servidor iniciado.");
				onReceiveCallback.accept("Ouvindo a porta " + port);
				
				// Loop do servidor esperando clientes
				while(true) {
					
					// Se achar um cliente ...
					Socket socketclient = server.accept();
					
					if(Thread.currentThread().isInterrupted())
						break;
					
					// Procura um espa�o livre
					int i;
					for(i = 0; i < num_client; i++)
					{
						if(client[i] == null) {
							onReceiveCallback.accept("Conectando novo cliente ...");
							client[i] = new ThreadConexaoCliente(i,socketclient,client,onReceiveCallback);
							client[i].start();
							break;
						}
					}
					// Se n�o achar avisa erro
					if (i == num_client)
						onReceiveCallback.accept("Servidor cheio ...");
				}
			}
			catch(Exception e) {
				onReceiveCallback.accept("Erro no servidor!");
				onReceiveCallback.accept("Erro: " + e.getMessage());
			}
		}
	}
}
