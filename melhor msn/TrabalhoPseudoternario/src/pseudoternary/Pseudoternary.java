package pseudoternary;

// Characteres n�o ASCII ser�o ignorados
// Char = 16 bits. Mas ASCII = 8 bits (1 bit n�o usado + 7 bits), ou seja, > 127
// Byte = 8 bits
// Cada bit � codificado em 2 bits no sinal de saida

/*
 * https://pt.wikipedia.org/wiki/ASCII
 * https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op3.html
 * https://stackoverflow.com/questions/33864306/how-to-convert-a-string-into-8-bit-ascii-in-java
 */

public class Pseudoternary {
	private static final byte low_signal = 0b0000_0011; 	// "11" -1 Para bit 0 alternando
	private static final byte error_signal = 0b0000_0010; 	// "10" -2 N�o existe
	private static final byte high_signal = 0b0000_0001; 	// "01" +1 Para bit 0 alternando
	private static final byte null_signal = 0b0000_0000; 	// "00" +0 Para bit 1
	
	public static byte[] encode(String message, boolean utf_8) {
		if(message.isEmpty())
			return null;
		// Create Output Signal
		int signal_indice = 0;
		byte[] signal = new byte[message.length() * 2 + 1];
		
		// Alternar entre 1 e -1
		boolean change_signal = false;
		
		for (char ch : message.toCharArray()) {
			//System.out.print(Integer.toBinaryString(ch));
			// Ignora todos caracteres nao ASCII
			if((int)ch > 127 && !utf_8 || (int)ch > 255 && utf_8)
				continue;
			
			// Char para byte, por no Java char tem 16 bits
			byte ch_ascii = (byte)ch;
			
			// Preparar os 2 bytes do sinal
			signal[signal_indice + 0] = 0x00;
			signal[signal_indice + 1] = 0x00;
			
			// Percorrer todos 8 bits desse byte
			for(int bit = 7; bit >= 0; bit--) {
				// Preciso de outro byte para o sinal de saida
				if(bit == 3)
				{
					signal_indice++;
				}
				// Encontrar o bit atual
				boolean current_bit = ( (byte)( 0x01 & (ch_ascii >>> bit) ) != 0x00 ? true : false );
				// Escolher o nivel do sinal para esse bit
				byte current_signal;
				if(current_bit) {
					current_signal = null_signal;
				}
				else {
					// Inverter a flag do -1 e 1 invertendo se usar
					current_signal = ( change_signal ? low_signal : high_signal );
					change_signal = !change_signal;
				}
				
				signal[signal_indice] = (byte) ( signal[signal_indice] | (current_signal << ((bit % 4)*2)) );
			}
			
			//System.out.print(" "+(signal_indice-1));
			//System.out.print(" "+Integer.toBinaryString(0x100 | 0xFF & (short)signal[signal_indice-1]));
			//System.out.print(" "+(signal_indice-0));
			//System.out.println(" "+Integer.toBinaryString(0x100 | 0xFF & (short)signal[signal_indice-0]));
			
			signal_indice++;
		}
		//System.out.println(" Size: "+(signal_indice));
		byte[] signal_final = new byte[signal_indice];

		for(int i = 0; i < signal_indice; i++) {
			signal_final[i] = signal[i];
		}
		
		return signal_final;
	}
	
	public static String decode(byte[] signal) {
		if(signal.length <= 0) {
			return null;
		}
		
		//System.out.println("");
		
		StringBuilder message = new StringBuilder();
		boolean change_signal = false;
		
		// Pega 2 byte
		for(int i = 0; i < signal.length; i+=2) {
			byte current_char_byte = 0x00;
			
			// Percorre os 16 bits 2 a 2
			for(int bit = 14; bit >= 0; bit-=2) {
				byte current_signal = error_signal;
				boolean signal_bit = false;
				// Metade superior - Usar signal[i]
				if(bit >= 7) {
					current_signal = (byte) ( 0x03 & ( (signal[ i ]) >>> (bit % 8) ) );
				}
				// Metade inferior - Usar signal[i+1]
				else if(i - 1 < signal.length) {
					current_signal = (byte) ( 0x03 & ( (signal[i+1]) >>> (bit % 8) ) );
				}
				
				//System.out.print(i + " " + bit + " " + current_signal + " ");
				
				if(current_signal == null_signal) {
					signal_bit = false;
				} 
				else if(current_signal == high_signal && !change_signal) {
					change_signal = true;
					signal_bit = true;
				}
				else if(current_signal == low_signal && change_signal) {
					change_signal = false;
					signal_bit = true;
				}
				else {
					//System.out.println("Erro ao reconhecer o sinal.");
					return null;
				}
				
				current_char_byte = !signal_bit ? (byte) ( current_char_byte | (0b0000_0001) << (short)(bit/2) ) : current_char_byte;
			}
			//System.out.println(" "+Integer.toBinaryString(0x100 | 0xFF & (short)current_char_byte));
			message.append( (char) ( 0x00FF & current_char_byte ) );
		}
		
		return message.toString();
	}
	
	public static String fixASCII(String no_ascii_string) {
		return Pseudoternary.decode(Pseudoternary.encode(no_ascii_string, false));
	}
}
